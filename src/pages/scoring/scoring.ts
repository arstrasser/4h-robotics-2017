import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ScoringPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-scoring',
  templateUrl: 'scoring.html',
})
export class ScoringPage {
  nutrients:{auto:{yv:number,yh:number,gv:number,gh:number,wv:number,wh:number},
            teleop:{yv:number,yh:number,gv:number,gh:number,wv:number,wh:number}} =
    {auto:{yv:0,yh:0,gv:0,gh:0,wv:0,wh:0},teleop:{yv:0,yh:0,gv:0,gh:0,wv:0,wh:0}};
  teleopMode:boolean = false;
  autoBlocksTouching:number = 0;
  fruit:{harvested:number,delivered:number,packaged:number} = {harvested:0,delivered:0,packaged:0};
  touchPenalties:number = 0;
  max = Math.max;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  clear(){
    if(window.confirm("Are you sure you would like to clear?")){
      this.nutrients = {auto:{yv:0,yh:0,gv:0,gh:0,wv:0,wh:0},teleop:{yv:0,yh:0,gv:0,gh:0,wv:0,wh:0}};
      this.teleopMode = false;
      this.autoBlocksTouching = 0;
      this.fruit = {harvested:0,delivered:0,packaged:0};
      this.touchPenalties = 0;
    }
  }

  autoToggle(){
    if(this.teleopMode == true){
      for(var obj in this.nutrients.teleop){
        if(this.nutrients.teleop.hasOwnProperty(obj) && this.nutrients.teleop[obj] != 0){
          return;
        }
      }
      this.nutrients.teleop = JSON.parse(JSON.stringify(this.nutrients.auto));
    }
  }

  getAutoSupersize() {
    let temp = JSON.parse(JSON.stringify(this.nutrients.auto));
    let total = 0;
    while(temp.yv+temp.yh >= 1 && temp.gv+temp.gh >= 1 && temp.wv+temp.wh >= 1){
      if(temp.yv > 0){
        total += 3;
        temp.yv--;
      }else{
        total += 1;
        temp.yh--;
      }

      if(temp.gv > 0){
        total += 3;
        temp.gv--;
      }else{
        total += 1;
        temp.gh--;
      }

      if(temp.wv > 0){
        total += 3;
        temp.wv--;
      }else{
        total += 1;
        temp.wh--;
      }
    }
    return total;
  }

  getTeleopSupersize() {
    let temp = JSON.parse(JSON.stringify(this.nutrients.teleop));
    let total = 0;
    while(temp.yv+temp.yh >= 1 && temp.gv+temp.gh >= 1 && temp.wv+temp.wh >= 1){
      if(temp.yv > 0){
        total += 3;
        temp.yv--;
      }else{
        total += 1;
        temp.yh--;
      }

      if(temp.gv > 0){
        total += 3;
        temp.gv--;
      }else{
        total += 1;
        temp.gh--;
      }

      if(temp.wv > 0){
        total += 3;
        temp.wv--;
      }else{
        total += 1;
        temp.wh--;
      }
    }
    return total;
  }

  autoTotal(){
    return (this.nutrients.auto.yv+this.nutrients.auto.gv+this.nutrients.auto.wv)*30+(this.nutrients.auto.yh+this.nutrients.auto.gh+this.nutrients.auto.wh)*10+this.autoBlocksTouching;
  }

  teleopTotal(){
    return (this.nutrients.teleop.yv+this.nutrients.teleop.gv+this.nutrients.teleop.wv)*30+
        (this.nutrients.teleop.yh+this.nutrients.teleop.gh+this.nutrients.teleop.wh)*10+
        this.fruit.harvested*3+this.fruit.delivered*8+this.fruit.packaged*20+
        Math.min(this.fruit.packaged, this.getAutoSupersize()+this.getTeleopSupersize())*25;
  }

}
